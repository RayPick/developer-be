package org.edgegallery.developer.model.deployyaml;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SecretKeyRef {
    private  String name;
    private  String key;
}
